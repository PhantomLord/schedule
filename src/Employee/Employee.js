import React from 'react';
import EmployeeInfo from './EmployeeInfo';
import Schedule from '../Schedule/Schedule';
import './Employee.css';

export default class Employee extends React.Component {
    constructor(props) {
        super();
        this.position = props.position;
        this.state = {
            data_range: props.data_range,
            start_date: props.start_date,
            employee_info: props.employee,
            schedule: props.schedule
        };
    }

    componentWillReceiveProps(nextProps) {
        //change position
        if (nextProps.position !== this.position)
            this.position = nextProps.position

        //change data_range
        if (nextProps.data_range !== this.state.data_range) {
            this.setState({
                data_range: nextProps.data_range
            });
        }

        //change start_date
        if (nextProps.start_date !== this.state.start_date) {
            this.setState({
                start_date: nextProps.start_date
            });
        }

        //change employee
        if (nextProps.employee !== this.state.employee_info) {
            this.setState({
                employee_info: nextProps.employee
            });
        }

        //change schedule
        if (nextProps.schedule !== this.state.schedule) {
            this.setState({
                schedule: nextProps.schedule
            });
        }
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.data_range !== this.state.data_range ||
                nextProps.start_date !== this.state.start_date ||
                nextProps.schedule !== this.state.schedule;
    }

    render() {
        return (
            <div className="employee">
                <EmployeeInfo name={this.state.employee_info.name}
                    position={this.position}
                    avatar={this.state.employee_info.avatar} />
                <Schedule data_range={this.state.data_range} start_date={this.state.start_date} employee={this.state.employee_info} schedule={this.state.schedule} />
            </div>
        );
    }
}