import React from 'react';
import Helpers from '../Helpers';
import './EmployeesListHeader.css'

export default class EmployeesListHeader extends React.Component {
    constructor(props) {
        super();
        this.state = {
            data_range : props.data_range,
            start_date : props.date
        };
    }

    componentWillReceiveProps(nextState) {
        if (nextState.data_range !== this.state.data_range) {
            this.setState({
                data_range: nextState.data_range
            });
        }
        if (nextState.date !== this.state.start_date) {
            this.setState({
                start_date: nextState.date
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.data_range !== this.state.data_range) {
            return true;
        }
        if (nextState.date !== this.state.start_date) {
            return true;
        }
        return false;
    }

    getRange(data_range){
        let range = "";
        let beginning_of_week = Helpers.getBeginningOfWeek(this.state.start_date);

        switch (data_range) {
            case "day" :
                range = (new Date(this.state.start_date)).toDateString();
                break;
            case "week" :
                range = (new Date(beginning_of_week)).toDateString() + " - " + (new Date(beginning_of_week + 3600000 * 24 * (7-1) )).toDateString();
                break;
            case "week2" :
                range = (new Date(beginning_of_week)).toDateString() + " - " + (new Date(beginning_of_week + 3600000 * 24 * (14-1)) ).toDateString();
                break;
            case "week4" :
                range = (new Date(beginning_of_week)).toDateString() + " - " + (new Date(beginning_of_week + 3600000 * 24 * (28-1)) ).toDateString();
                break;
            default :
                range = (new Date(beginning_of_week)).toDateString() + " - " + (new Date(beginning_of_week + 3600000 * 24 * 14-1)).toDateString();
                break;
        }

        return range;
    }

    generateElements(days) {
        const elems_num = days !== "day" ? days : 24;
        let fields = [];

        if( days !== "day" ) {
            let beginning_of_week = Helpers.getBeginningOfWeek(this.state.start_date);

            for (let i = 0; i < elems_num; i++) {
                let date = new Date(beginning_of_week + i * (1000 * 60 * 60 * 24));
                date.setHours(0, 0, 0, 0);

                fields.push(
                    <div id={"header-" + this.state.data_range + "-" + i}
                         className={"day_field " + this.state.data_range + "_day_field"}
                         key={"day_field_" + i}>
                        { date.toDateString() }
                    </div>
                );
            }
        } else {
            for (let i = 0; i < elems_num; i++) {
                fields.push(
                    <div id={"header-" + this.state.data_range + "-" + i}
                         className={"day_field " + this.state.data_range + "_day_field"}
                         key={"day_field" + i}>
                        { i.toString().length === 1 ? "0"+i : i }
                    </div>
                );
            }
        }

        return fields;
    }

    render() {
        let dates = "";

        switch (this.state.data_range) {
            case "day":
                dates = this.generateElements("day");
                break;
            case "week":
                dates = this.generateElements(7);
                break;
            case "week2":
                dates = this.generateElements(14);
                break;
            case "week4":
                dates = this.generateElements(28);
                break;
            default:
                this.setState({ data_range: "week2" })
                dates = this.generateElements(14);
                break;
        }

        return (
            <div className="employees_list_header">
                <div className="employees_schedule_range">{this.getRange(this.state.data_range)}</div>
                <div className="employee_schedule_date">{dates}</div>
            </div>
        );
    }
}