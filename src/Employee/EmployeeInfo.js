import React from 'react';
import './EmployeeInfo.css';

export default class EmployeeInfo extends React.Component {
    constructor(props) {
        super();
        this.name = props.name;
        this.avatar = props.avatar;
        this.position = props.position;
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.name !== this.name)
            this.name = nextProps.name;

        if(nextProps.avatar !== this.avatar)
            this.avatar= nextProps.avatar;

        if(nextProps.position !== this.position)
            this.position = nextProps.position;
    }

    render() {
        return (
            <div className="employee_info">
                <img src={this.avatar} alt={this.name} />
                <div className="employee_data">
                    <div className="employee_name">{this.name}</div>
                    <div className="employee_position">{this.position}</div>
                </div>
            </div>
        );
    }
}