import React from 'react';
import Employee from './Employee';
import EmployeesListHeader from './EmployeesListHeader';
import {Link} from 'react-router-dom';
import Helpers from '../Helpers';
import PropTypes from 'prop-types';
import './EmployeesList.css';

export default class EmployeesList extends React.Component {
    constructor(props) {
        super(props);
        this.employees = props.employees;
        this.schedule = props.schedule;
        this.positions = props.positions;

        this.state = {
            data_range: "week2",
            start_date: Helpers.getBeginningOfWeek(),
            day_start_date: Helpers.today(4),
            display_schedule: this.schedule,
            display_employees: this.employees
        };
    }

    previousDate(current_date, data_range){
        let new_date = current_date;
        let day = 1000*3600*24;

        switch (data_range) {
            case 'day':
                new_date = current_date - day;
                break;
            case 'week':
                new_date = current_date - day*7;
                break;
            case 'week2':
                new_date = current_date - day*14;
                break;
            case 'week4':
                new_date = current_date - day*28;
                break;
            default:
                break;
        }

        this.changeDateState(new_date);
    }

    nextDate(current_date, data_range){
        let new_date = current_date;
        let day = 1000*3600*24;

        switch (data_range) {
            case 'day':
                new_date = current_date + day;
                break;
            case 'week':
                new_date = current_date + day*7;
                break;
            case 'week2':
                new_date = current_date + day*14;
                break;
            case 'week4':
                new_date = current_date + day*28;
                break;
            default:
                break;
        }

        this.changeDateState(new_date);
    }

    changeDateState(new_date){
        this.state.data_range === "day" ? this.setState({ day_start_date: new_date }) : this.setState({ start_date: new_date });
    }

    changeDataRange(data_range){
        this.setState({data_range: data_range});
    }

    changeDate(prev_next){
        switch (prev_next) {
            case 'prev':
                this.state.data_range === "day" ? this.previousDate(this.state.day_start_date, this.state.data_range) : this.previousDate(this.state.start_date, this.state.data_range);
                break;
            case 'next':
                this.state.data_range === "day" ? this.nextDate(this.state.day_start_date, this.state.data_range) : this.nextDate(this.state.start_date, this.state.data_range);
                break;
            case 'today':
                this.state.data_range === "day" ? ( this.state.day_start_date !== Helpers.today(4) ? this.changeDateState(Helpers.today(4)) : "" ) : this.changeDateState(Helpers.getBeginningOfWeek());
                break;
            default:
                break;
        }
    }

    getShiftByEmployeeId(id){
        let shifts = [];
        for(let shift of this.state.display_schedule){
            if( shift.employees.includes(id) )
                shifts.push(shift);
        }

        return shifts;
    }

    generatePositionsFilter(){
        let positions = [];
        for(let position of this.positions) {
            positions.push( <span className="position_filed" key={"position_"+position.id} style={ {"backgroundColor": position.color} }>
                                <label htmlFor={"position_"+position.id}>{ position.position}</label>
                                <input id={"position_"+position.id} name="positions_group[]" type="checkbox" value={position.id} onChange={this.filter.bind(this)} />
                            </span>);
        }

        return positions;
    }

    filter(){
        let checkboxes = document.getElementsByName("positions_group[]");
        let filter = [];
        for(let cb of checkboxes) {
            if(cb.checked)
                filter.push(parseInt(cb.value, 10))
        }

        if(filter.length > 0) {
            let temp_employees = [], temp_schedule = [], temp_employees_ids = [];
            for (let shift of this.schedule) {
                if (filter.includes(shift.position)) {
                    temp_schedule.push(shift)
                    for(let employee of this.employees){
                        if(shift.position === employee.position && shift.employees.includes(employee.id) && !temp_employees_ids.includes(employee.id)) {
                            temp_employees.push(employee);
                            temp_employees_ids.push(employee.id)
                        }
                    }
                }
            }
            this.setState({display_employees: temp_employees});
            this.setState({display_schedule: temp_schedule});
        } else {
            this.setState({display_employees: this.employees});
            this.setState({display_schedule: this.schedule});
        }
    }

    /*shouldComponentUpdate(nextState){
        return nextState.display_schedule !== this.state.display_schedule;
    }*/

    render(){
        const element_data = [];
        let key = 0;

            for (let employee of this.state.display_employees) {
                let p = this.positions.find( function(value) { return value.id === employee.position });
                element_data.push(
                    <Employee employee={employee}
                              schedule={ this.getShiftByEmployeeId(employee.id) }
                              key={"employee_" + key++}
                              start_date={this.state.data_range === "day" ? this.state.day_start_date : this.state.start_date}
                              data_range={this.state.data_range}
                              position={ p.position }/>
                );
            }

        return (
            <div className="employees_list">
                <div className="buttons">
                    <div className="buttons_set change_data_range">
                        <button onClick={this.changeDataRange.bind(this,'day')}><Link to={"/day"}>Day</Link></button>
                        <button onClick={this.changeDataRange.bind(this,'week')}><Link to={"/week"}>Week</Link></button>
                        <button onClick={this.changeDataRange.bind(this,'week2')}><Link to={"/week2"}>2 Week</Link></button>
                        <button onClick={this.changeDataRange.bind(this,'week4')}><Link to={"/week4"}>4 Week</Link></button>
                    </div>
                    <div className="buttons_set change_date">
                        <button onClick={this.changeDate.bind(this,'prev')}><Link to={"/"+this.state.data_range+"/"+ this.state.start_date }>Previous</Link></button>
                        <button onClick={this.changeDate.bind(this,'today')}><Link to={"/"+this.state.data_range+"/"+ this.state.start_date }>Today</Link></button>
                        <button onClick={this.changeDate.bind(this,'next')}><Link to={"/"+this.state.data_range+"/"+ this.state.start_date }>Next</Link></button>
                    </div>
                </div>
                <div className="positions_filter">
                    <div className="positions_filter_title">Filter employees by position</div>
                    { this.generatePositionsFilter() }
                </div>
                <hr/>
                <EmployeesListHeader date={this.state.data_range === "day" ? this.state.day_start_date : this.state.start_date} data_range={this.state.data_range} />
                <div className="employees_list_wrapper">{element_data}</div>
            </div>
        );
    }
}

EmployeesList.propTypes = {
    employees: PropTypes.array.isRequired,
    schedule: PropTypes.array.isRequired
};