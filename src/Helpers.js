export default class Helpers {

    static getBeginningOfWeek() {
        const today = arguments.length ? new Date(arguments[0]) : new Date();
        let monday = ( new Date( today.getTime() - (today.getDay() !== 0 ? today.getDay() - 1 : 6) * (1000*60*60*24) )).setHours(4,0,0,0);

        return monday;
    }

    static getDays() {
        return [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
    }
    static getWeekdays() {
        return [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" ];
    }

    static isWeekday(day) {
        let value = day;
        let exists = false;
        Helpers.getWeekdays().map(function(day) {
            if( day.indexOf(value) >= 0 )
                exists = true;
            return true;
        } );

        return exists;
    }

    //return today's date in milliseconds and set hour (4)
    static today(hour) {
        let h = hour ? hour : 0;
        return (new Date()).setHours(h,0,0,0);
    }

    /*static generateShifts(id, start_hour, start_min, end_hour, end_min, position, shift_name, employees, exclude) {
        let num = 50;
        let shifts = [];
        let start_date = 1489975200000;
        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        for (let i = 0; i <= num; i++) {
            let date = new Date(start_date + i * (1000 * 60 * 60 * 24));
            if ( !exclude.includes(date.getDay()) ) {
                let shift_obj = {
                    id: i+id,
                    position: position,
                    start_date: {
                        date: "2017-" + ( (date.getMonth() + 1).toString().length === 1 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1) ) + "-" + ( date.getDate().toString().length === 1 ? "0" + date.getDate() : date.getDate() ) + " " + ( start_hour.toString().length === 1 ? "0" + start_hour : start_hour ) + ":" + ( start_min.toString().length === 1 ? "0" + start_min : start_min ) + ":00",
                        formatted: months[date.getMonth()] + " " + ( date.getDate().toString().length === 1 ? "0" + date.getDate() : date.getDate() ) + ", 2017",
                        time: ( start_hour.toString().length === 1 ? "0" + start_hour : start_hour ) + ":" + ( start_min.toString().length === 1 ? "0" + start_min : start_min ),
                        timestamp: date.getTime(),
                        hours: start_hour,
                        minutes: start_min,
                    },
                    end_date: {
                        date: "2017-" + ( (date.getMonth() + 1).toString().length === 1 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1) ) + "-" + ( date.getDate().toString().length === 1 ? "0" + date.getDate() : date.getDate() ) + " " + ( end_hour.toString().length === 1 ? "0" + end_hour : end_hour ) + ":" + ( end_min.toString().length === 1 ? "0" + end_min : end_min ) + ":00",
                        formatted: months[date.getMonth()] + " " + ( date.getDate().toString().length === 1 ? "0" + date.getDate() : date.getDate() ) + ", 2017",
                        time: ( end_hour.toString().length === 1 ? "0" + end_hour : end_hour ) + ":" + ( end_min.toString().length === 1 ? "0" + end_min : end_min ),
                        timestamp: date.getTime(),
                        hours: end_hour,
                        minutes: end_min,
                    },
                    title: "",
                    schedule_name: shift_name,
                    employees: employees
                };
                shifts.push(shift_obj);
            }
        }
        window.shifts = shifts;
        return shifts;
    }*/
    //generateShifts(18,30,0,30,4,"Social Media",[6, 8], [1, 2]);
}