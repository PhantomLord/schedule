import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import EmployeesList from './Employee/EmployeesList';

const positions = [
    {
        id: 1,
        position: "Design",
        color: "#FFEB3B"
    },
    {
        id: 2,
        position: "Content",
        color: "#FAA2C1"
    },
    {
        id: 3,
        position: "Development",
        color: "#91A7FF"
    },
    {
        id: 4,
        position: "Social Media",
        color: "#BE4BDB"
    },
    {
        id: 5,
        position: "Analytics",
        color: "#868E96"
    },
];

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            employees: null,
            schedule: null
        };
    }

    componentWillMount() {
        fetch('http://localhost:3004/employees', {headers: {'Content-Type': 'application/json'}})
            .then(resp => resp.json())
            .then(json => this.setState({employees: json}));
        fetch('http://localhost:3004/schedule', {headers: {'Content-Type': 'application/json'}})
            .then(resp => resp.json())
            .then(function (shifts) {
                this.setState({schedule: shifts})
            }.bind(this));
    }

    render() {
        if (!this.state.employees || !this.state.schedule) {
            return <div>Loading data...</div>;
        }
        return (
            <Router>
                <EmployeesList employees={this.state.employees} schedule={this.state.schedule} positions={positions} />
            </Router>
        );
    }

}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);
