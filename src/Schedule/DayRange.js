import React from 'react';
import Shift from './Shift';
import './DayRange.css';

export default class DayRange extends React.Component {
    constructor(props) {
        super();
        this.number_of_fields = 24;
        this.employee = props.employee;
        this.schedule = props.schedule;
        this.date = props.date;

        this.state = {
            shift: null,
            shift_element : null
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.employee !== this.employee)
            this.employee = nextProps.employee;

        if(nextProps.schedule !== this.schedule) {
            this.schedule = nextProps.schedule;
            this.setShift();
        }

        if (nextProps.date !== this.date) {
            this.date = nextProps.date;
            this.setShift();
        }
    }

    shiftPosition(shift){
        if(shift) {
            let start_hour = shift.start_date.hours;
            let start_minute = shift.start_date.minutes;

            let end_hour = shift.end_date.hours;
            let end_minute = shift.end_date.minutes;

            let start_element = document.getElementById(this.employee.id + "-hour-" + parseInt(start_hour, 10));
            let left = start_element.offsetLeft + start_minute * start_element.offsetWidth/60;

            let end_element = "";
            let width = 0;
            if(this.state.shift.start_date.hours > this.state.shift.end_date.hours) {
                end_element = document.getElementById(this.employee.id + "-hour-23");
                width = (end_element.offsetLeft + end_element.offsetWidth) - left;
            } else {
                end_element = document.getElementById(this.employee.id + "-hour-" + parseInt(end_hour, 10));
                width = (end_element.offsetLeft + end_minute * end_element.offsetWidth/60) - left;
            }

            return {"left":left, "width":width};
        }
    }

    componentWillMount(){
        this.setShift();
        //console.log(this.employee.id + " | " + this.employee.name + " MOUNTED on position ");
    }

    /*componentWillUnmount() {
        console.log(this.employee.id + " | " + this.state.shift.schedule_name.toUpperCase() + "component unmounted")
        //this.setState({ shift: null });
    }*/



    generateFields(){
        let fields = [];

        for (let i = 0; i < this.number_of_fields; i++)
            fields.push(<div
                id={this.employee.id + "-hour-" + i}
                className="day_hour_field" data-hour={i}
                key={"day_hour_field_" + i}>
                { /*(new Date(this.date)).getDate()*/ }
            </div>);
        fields.push(this.state.shift_element);

        return fields;
    }

    getTodayShift(date) {
        let shift = null;
        for(let s of this.schedule) {
            if(s.start_date.timestamp === date) {
                shift = s;
                break;
            }
        }

        return shift;
    }

    generateShiftElement(){
        if(this.state.shift !== null){
            let shift = this.state.shift;

            this.setState({
                shift_element: <Shift shift={shift} team={this.employee.team}
                                      type="day_shift"
                                      position={this.shiftPosition(shift)}
                                      key={this.employee.id + "-shift-" + shift.id} />
            });
        } else {
            this.setState({
                shift_element: null
            });
        }
    }

    setShift() {
        this.setState({
            shift: this.getTodayShift(this.date)
        }, function(){
            this.generateShiftElement();
        });
    }

    render(){
        return (
            <div className="day_range_view">
                { this.generateFields() }
            </div>
        );
    }
}