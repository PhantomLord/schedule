import React from 'react';
import Field from './Field';
import Helpers from '../Helpers';
import PropTypes from 'prop-types';

export default class WeekRange extends React.Component {
    constructor(props) {
        super();
        this.state = {
            range: props.range,
            start_date: props.start_date,
            schedule: props.schedule
        };
        this.days_num = {"week" : 7, "week2" : 14, "week4" : 28};
        this.employee = props.employee;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.employee !== this.employee)
            this.employee = nextProps.employee;

        if (nextProps.range !== this.state.range) {
            this.setState({
                range: nextProps.range
            });
        }
        if (nextProps.start_date !== this.state.start_date) {
            this.setState({
                start_date: nextProps.start_date
            });
        }
        if (nextProps.schedule !== this.state.schedule) {
            this.setState({
                schedule: nextProps.schedule
            });
        }
    }

    /*shouldComponentUpdate(nextProps) {
        return nextProps.employee !== this.employee ||
                nextProps.schedule !== this.state.schedule;
    }*/

    addShift(date){
        let shift;

        for(let s of this.state.schedule)
            if(s.start_date.timestamp === date)
                shift = s;
        return shift;
    }

    generateElements() {
        let fields = [];

        for (let i = 0; i < this.days_num[this.state.range]; i++) {
            let date = this.state.start_date + i * 86400000;
            fields.push(<Field id={this.employee.id + "-" + this.state.range + "-" + i}
                               class={"day_field " + this.state.range + "_day_field " + ( Helpers.today(4) === date ? "today" : "")}
                               data_date={date}
                               key={this.employee.id + "-" + this.state.range + "-" + i}
                               ref={field => this.fieldId = field}
                               shift={this.addShift(date)} />);
        }

        return fields;
    }

    render(){
        return <div className="employee_schedule">{ this.generateElements() }</div>;
    }

}

WeekRange.propTypes = {
    employee: PropTypes.object.isRequired
};