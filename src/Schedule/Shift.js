import React from 'react';
import PropTypes from 'prop-types';
import Helpers from '../Helpers';
import './Shift.css';

export default class Shift extends React.Component {
    constructor(props) {
        super();
        this.shift = props.shift;
        this.type = props.type;
        this.team = props.team;
        props.position ? this.position = props.position : this.position = { left: 5, width: 100};
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.shift !== this.shift) {
            this.shift = nextProps.shift;
            //this.forceUpdate();
        }
    }

    /*shouldComponentUpdate(nextProps) {
        return nextProps.shift !== this.shift;
    }*/

    generateShift() {
        return (
            <span
                className={ "shift " + ( this.shift.schedule_name ? this.shift.schedule_name.replace(' ', '_').toLowerCase() + "_shift" : "default_shift" ) + ( this.type ? " " + this.type : "") + (this.shift.start_date.timestamp < Helpers.today(4) ? " past" : "") }
                style={ this.position ? {
                    "left": this.position.left,
                    "width": this.position.width
                } : {}
                }>
                <span className="team">{this.shift.schedule_name}</span>
                <span className="start_time">{this.shift.start_date.time}</span><span
                className="separator">-</span><span className="end_time">{this.shift.end_date.time}</span>
                { /*this.shift.start_date.formatted*/ }
            </span>
        );
    }

    render() {
        return this.generateShift();
    }
}

Shift.propTypes = {
    shift: PropTypes.object.isRequired,
    type: PropTypes.string,
    team: PropTypes.string,
    position: PropTypes.object
};