import React from 'react';
import DayRange from './DayRange';
import WeekRange from './WeekRange';
import PropTypes from 'prop-types';
//import Route from 'react-router-dom';

export default class Schedule extends React.Component {
    constructor(props) {
        super();
        this.state = {
            data_range: props.data_range,
            start_date: props.start_date,
            schedule: props.schedule
        };
        this.employee = props.employee;
    }

    componentWillReceiveProps(nextProps) {
        //change data_range
        if (nextProps.data_range !== this.state.data_range) {
            this.setState({
                data_range: nextProps.data_range
            });
        }

        //change start_date
        if (nextProps.start_date !== this.state.start_date) {
            this.setState({
                start_date: nextProps.start_date
            });
        }

        //change schedule
        if (nextProps.schedule !== this.state.schedule) {
            this.setState({
                schedule: nextProps.schedule
            });
        }

        //change employee
        if(nextProps.employee !== this.employee)
            this.employee = nextProps.employee;
    }

    /*shouldComponentUpdate(nextProps) {
        return nextProps.data_range !== this.state.data_range ||
                nextProps.start_date !== this.state.start_date ||
                nextProps.schedule !== this.state.schedule ||
                nextProps.employee !== this.employee;
    }*/

    render() {
        let elements = "";
        switch (this.state.data_range) {
            case "day":
                elements = <DayRange employee={this.employee} schedule={this.state.schedule} date={this.state.start_date} />
                break;
            default:
                //this.setState({ data_range: "week2" })
                //elements = <WeekRange employee={this.employee} range="week2" start_date={this.state.start_date} />;
                elements = <WeekRange employee={this.employee} schedule={this.state.schedule} range={this.state.data_range} start_date={this.state.start_date} />;
                break;
        }

        return <div className="employee_schedule">{elements}</div>;
    }
}

Schedule.propTypes = {
    employee: PropTypes.object.isRequired
};