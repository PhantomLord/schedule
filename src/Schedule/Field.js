import React from 'react';
import Shift from './Shift';

export default class Field extends React.Component {
    constructor(props) {
        super();
        this.id = props.id;
        this.state = {
            data_date: props.data_date,
            class: props.class,
            shift: props.shift
        }
    }

    componentWillReceiveProps(nextState) {
        if (nextState.data_date !== this.state.data_date) {
            this.setState({
                data_date: nextState.data_date
            });
        }
        if (nextState.class !== this.state.class) {
            this.setState({
                class: nextState.class
            });
        }
        if (nextState.shift !== this.state.shift) {
            this.setState({
                shift: nextState.shift
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.data_date !== this.state.data_date || nextState.class !== this.state.class;
    }

    componentWillMount() {

    }

    render(){
        return <div id={this.id} className={this.state.class}>
                { this.state.shift ? <Shift shift={this.state.shift} /> : "" }
                { /*(new Date(this.state.data_date)).getDate()*/ }
            </div>;
    }
}
